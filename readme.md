# Website Searcher

This is wework's take home assignment.

## How to run

1. Download the `wework.jar` and `urls.txt` files in the same directory.
2. Open a terminal and execute `java -jar ./wework.jar`.
3. Once the program finishes it will generate `results.txt` with the results.

## Requirements

JRE 8 or above

## Performance

It will vary based on hardware and network but in my case it takes around 28 seconds to run this program with a connection and read timeout of 5 and 10 seconds respectively, decreasing them will run it faster but then you will have more timeouts.
