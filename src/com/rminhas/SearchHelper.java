package com.rminhas;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.util.regex.Pattern;

/***
 * 
 * @author Rizwan Minhas
 *
 */

public class SearchHelper {
	/**
	 * Fetches the provided URL content.
	 * 
	 * @param url
	 *            URL that needs to fetched.
	 * @return the content of the URL or an empty string in case of an exception.
	 */
	public static String fetchURLContent(String url) {
		System.out.println(Thread.currentThread().getName() + " searching -> " + url);
		Scanner scanner = null;
		try {
			URLConnection urlConnection = new URL(url).openConnection();
			urlConnection.setRequestProperty("User-Agent",
					"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3769.0 Safari/537.36");
			urlConnection.setConnectTimeout(5000);
			urlConnection.setReadTimeout(10000);
			scanner = new Scanner(urlConnection.getInputStream(), StandardCharsets.UTF_8.toString());
			scanner.useDelimiter("\\A");
			return scanner.hasNext() ? scanner.next() : "";
		} catch (IOException ioe) {
			return "";
		} finally {
			if (scanner != null) {
				scanner.close();
			}
		}
	}

	/**
	 * Searches for a word on the provided domain.
	 * 
	 * @param domain
	 *            The domain that needs to be searched
	 * @param searchWordPattern
	 *            the {@link java.util.regex.Pattern} of the word that will be
	 *            searched in the URL
	 * @return The {protocol + URL} where the word was found or an empty string if
	 *         the search was unsuccessful
	 */
	public static String isMatch(String domain, Pattern searchWordPattern) {
		String httpUrl = "http://www." + domain;
		String httpContent = fetchURLContent(httpUrl);
		if ("".equals(httpContent)) {
			String httpsUrl = "https://www." + domain;
			return searchWordPattern.matcher(fetchURLContent(httpsUrl)).find() ? httpsUrl : "";
		} else {
			return searchWordPattern.matcher(httpContent).find() ? httpUrl : "";
		}
	}
}