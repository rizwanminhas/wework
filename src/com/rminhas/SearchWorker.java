package com.rminhas;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

/***
 * 
 * @author Rizwan Minhas
 *
 */
public class SearchWorker implements Runnable {
	private final AtomicInteger domainIndex;
	private final Pattern searchPattern;
	private final List<String> domains;
	private final List<String> result;

	public SearchWorker(List<String> domains, List<String> result, String searchWord) {
		this.domainIndex = new AtomicInteger(0);
		this.domains = domains;
		this.result = result;
		this.searchPattern = Pattern.compile(searchWord, Pattern.CASE_INSENSITIVE);
	}

	public void run() {
		while (true) {
			if (domainIndex.get() < domains.size()) {
				String domain = domains.get(domainIndex.getAndIncrement());
				String url = SearchHelper.isMatch(domain, searchPattern);
				if (!"".equals(url)) {
					result.add(url);
				}
			} else {
				System.out.println("Finishing - " + Thread.currentThread().getName());
				break;
			}
		}
	}
}