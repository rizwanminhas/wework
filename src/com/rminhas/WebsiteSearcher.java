package com.rminhas;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/***
 * 
 * @author Rizwan Minhas
 * 
 *         Wework's take home challenge.
 *
 */

public class WebsiteSearcher {
	private static final int THREAD_POOL_SIZE = 20;

	public static void main(String[] args) {
		List<String> domains = extractDomainsToSearch("urls.txt");
		List<String> results = new LinkedList<String>();

		long start = System.nanoTime();
		List<Thread> threads = startThreads(domains, results, "weather");
		joinThreads(threads);

		System.out.println("Time taken (in nanoseconds): " + (System.nanoTime() - start));
		System.out.println("Match found in " + results.size() + " websites. Check results.txt for urls.");

		writeToFile("results.txt", results);
	}

	/**
	 * Extract the domain names from the provided csv file, skips the first header
	 * line and removes the quotes around the domain name
	 * 
	 * @param fileName
	 *            the file where the domain names exist for searching purposes.
	 * @return a {@link java.util.List} of domain names
	 */
	private static List<String> extractDomainsToSearch(String fileName) {
		try (Stream<String> stream = Files.lines(Paths.get(fileName), StandardCharsets.UTF_8)) {
			return stream.skip(1).parallel().map(line -> line.split(",")[1].replaceAll("\"", ""))
					.collect(Collectors.toList());
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		return Collections.emptyList();
	}

	/**
	 * Starts the threads
	 * 
	 * @param domains domains that will be searched
	 * @param results this will contain the URLs where the word is found
	 * @param searchTerm the word that needs to be found
	 * @return a list of threads
	 */
	private static List<Thread> startThreads(List<String> domains, List<String> results, String searchTerm) {
		List<Thread> threads = new LinkedList<Thread>();
		SearchWorker searchWorker = new SearchWorker(domains, results, searchTerm);
		for (int i = 0; i < THREAD_POOL_SIZE; i++) {
			Thread workerThread = new Thread(searchWorker, "t" + i);
			workerThread.start();
			threads.add(workerThread);
		}
		return threads;
	}

	/**
	 * Pauses the main thread so the other 20 threads can finish their search tasks and populate the results
	 * 
	 * @param threads
	 */
	private static void joinThreads(List<Thread> threads) {
		for (int i = 0; i < THREAD_POOL_SIZE; i++) {
			try {
				threads.get(i).join();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private static void writeToFile(String fileName, List<String> list) {
		try {
			Files.write(Paths.get(fileName), list, StandardCharsets.UTF_8);
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
}